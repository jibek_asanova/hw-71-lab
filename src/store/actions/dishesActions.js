import axiosApi from "../../axiosApi";

export const ADD_DISHES_REQUEST = 'ADD_DISHES_REQUEST';
export const ADD_DISHES_SUCCESS = 'ADD_DISHES_SUCCESS';
export const ADD_DISHES_FAILURE = 'ADD_DISHES_FAILURE';

export const GET_DISHES_REQUEST = 'GET_DISHES_REQUEST';
export const GET_DISHES_SUCCESS = 'GET_DISHES_SUCCESS';
export const GET_DISHES_FAILURE = 'GET_DISHES_FAILURE';

export const DELETE_DISH_REQUEST = 'DELETE_DISH_REQUEST';
export const DELETE_DISH_SUCCESS = 'DELETE_DISH_SUCCESS';
export const DELETE_DISH_FAILURE = 'DELETE_DISH_FAILURE';

export const FETCH_EDIT_DISHES_REQUEST = 'FETCH_EDIT_DISHES_REQUEST';
export const FETCH_EDIT_DISHES_SUCCESS = 'FETCH_EDIT_DISHES_SUCCESS';
export const FETCH_EDIT_DISHES_FAILURE = 'FETCH_EDIT_DISHES_FAILURE';

export const EDIT_DISH_REQUEST = 'EDIT_DISH_REQUEST';
export const EDIT_DISH_SUCCESS = 'EDIT_DISH_SUCCESS';
export const EDIT_DISH_FAILURE = 'EDIT_DISH_FAILURE';

export const ADD_PRODUCT_TO_CARD = 'ADD_PRODUCT_TO_CARD';


export const addDishesRequest = () => ({type: ADD_DISHES_REQUEST});
export const addDishesSuccess = dishes => ({type: ADD_DISHES_SUCCESS, payload: dishes});
export const addDishesFailure = () => ({type: ADD_DISHES_FAILURE});

export const getDishesRequest = () => ({type: GET_DISHES_REQUEST});
export const getDishesSuccess = dishes => ({type: GET_DISHES_SUCCESS, payload: dishes});
export const getDishesFailure = () => ({type: GET_DISHES_FAILURE});

export const deleteDishRequest = () => ({type: DELETE_DISH_REQUEST});
export const deleteDishSuccess = dish => ({type: DELETE_DISH_SUCCESS, payload: dish});
export const deleteDishFailure = () => ({type: DELETE_DISH_FAILURE});

export const editDishRequest = () => ({type: EDIT_DISH_REQUEST});
export const editDishSuccess = dish => ({type: EDIT_DISH_SUCCESS, payload: dish});
export const editDishFailure = () => ({type: EDIT_DISH_FAILURE});

export const fetchEditDishRequest = () => ({type: FETCH_EDIT_DISHES_REQUEST});
export const fetchEditDishSuccess = dishes => ({type: FETCH_EDIT_DISHES_SUCCESS, payload: dishes});
export const fetchEditDishFailure = () => ({type: FETCH_EDIT_DISHES_FAILURE});



export const addNewDish = dishData => {
    return async dispatch => {
        try {
            dispatch(addDishesRequest());
            await axiosApi.post('/dishes.json', dishData);
            dispatch(addDishesSuccess());
        } catch (error) {
            dispatch(addDishesFailure(error));
            throw error;
        }
    }
};

export const getDishes = () => {
    return async (dispatch) => {
        dispatch(getDishesRequest());
        try {
            const response = await axiosApi.get('/dishes.json');
            const dishesList = response.data;
            dispatch(getDishesSuccess(dishesList));
        } catch (error) {
            dispatch(getDishesFailure(error));
        }
    }
};

export const deleteDish = (id) => {
    return async (dispatch) => {
        dispatch(deleteDishRequest());
        try {
            const response = await axiosApi.delete('/dishes/' + id + '.json');
            dispatch(deleteDishSuccess(response.data));
            dispatch(getDishes());
        } catch(error) {
            dispatch(deleteDishFailure(error));
        }
    }
};

export const editDishGetRequest = (id) => {
    return async (dispatch) => {
        dispatch(fetchEditDishRequest());
        try {
            const response = await axiosApi.get('/dishes/' + id + '.json');
            dispatch(fetchEditDishSuccess(response.data));
        } catch(error) {
            dispatch(fetchEditDishFailure(error));
        }
    }
};

export const editDish = (dishData, id) => {
    return async(dispatch) => {
        dispatch(editDishRequest());
        try {
            await axiosApi.put('/dishes/' + id + '.json', dishData);
            dispatch(editDishSuccess());
        } catch (error) {
            dispatch(editDishFailure());
        }
    }
};