import axiosApi from "../../axiosApi";


export const ADD_PRODUCT_TO_CARD = 'ADD_PRODUCT_TO_CARD';
export const REMOVE_PRODUCT_FROM_CARD = 'REMOVE_PRODUCT_FROM_CARD';
export const CREATE_ORDER_REQUEST = 'CREATE_ORDER_REQUEST';
export const CREATE_ORDER_SUCCESS = 'CREATE_ORDER_SUCCESS';
export const CREATE_ORDER_FAILURE = 'CREATE_ORDER_FAILURE';


export const addProductToCard = order => ({type: ADD_PRODUCT_TO_CARD, payload: order});
export const removeProductFromCard = order => ({type: REMOVE_PRODUCT_FROM_CARD, payload: order});
export const createOrderRequest = () => ({type: CREATE_ORDER_REQUEST});
export const createOrderSuccess = orders => ({type: CREATE_ORDER_SUCCESS, payload: orders});
export const createOrderFailure = () => ({type: CREATE_ORDER_FAILURE});


export const createOrder = orderData => {
    return async dispatch => {
        try {
            dispatch(createOrderRequest())
            await axiosApi.post('/orders.json', orderData);
            dispatch(createOrderSuccess());
        } catch (error) {
            dispatch(createOrderFailure(error));
            throw error;
        }
    }
}