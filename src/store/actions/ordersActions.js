import axiosApi from "../../axiosApi";

export const FETCH_ORDER_REQUEST = 'FETCH_ORDER_REQUEST';
export const FETCH_ORDER_SUCCESS = 'FETCH_ORDER_SUCCESS';
export const FETCH_ORDER_FAILURE = 'FETCH_ORDER_FAILURE';
export const DELETE_ORDER_REQUEST = 'DELETE_ORDER_REQUEST';
export const DELETE_ORDER_SUCCESS = 'DELETE_ORDER_SUCCESS';
export const DELETE_ORDER_FAILURE = 'DELETE_ORDER_FAILURE';

export const fetchOrderRequest = () => ({type: FETCH_ORDER_REQUEST});
export const fetchOrderSuccess = orders => ({type: FETCH_ORDER_SUCCESS, payload: orders});
export const fetchOrderFailure = () => ({type: FETCH_ORDER_FAILURE});

export const deleteOrderRequest = () => ({type: DELETE_ORDER_REQUEST});
export const deleteOrderSuccess = order => ({type: DELETE_ORDER_SUCCESS, payload: order});
export const deleteOrderFailure = () => ({type: DELETE_ORDER_FAILURE});

export const getOrder = () => {
    return async (dispatch) => {
        dispatch(fetchOrderRequest());
        try {
            const response = await axiosApi.get('/orders.json');
            const ordersList = response.data;
            dispatch(fetchOrderSuccess(ordersList));
        } catch (error) {
            dispatch(fetchOrderFailure(error));
        }
    }
};

export const deleteOrder = (id) => {
    return async (dispatch) => {
        dispatch(deleteOrderRequest());
        try {
            const response = await axiosApi.delete('/orders/' + id + '.json');
            dispatch(deleteOrderSuccess(response.data));
            dispatch(getOrder());
        } catch(error) {
            dispatch(deleteOrderFailure(error));
        }
    }
};