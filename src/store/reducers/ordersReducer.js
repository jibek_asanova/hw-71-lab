import {
    DELETE_ORDER_FAILURE,
    DELETE_ORDER_REQUEST, DELETE_ORDER_SUCCESS,
    FETCH_ORDER_FAILURE,
    FETCH_ORDER_REQUEST,
    FETCH_ORDER_SUCCESS
} from "../actions/ordersActions";

const initialState = {
    ordersList: [],
    loading:false,
    error: null,
};

const reducer = (state = initialState, action) => {
    switch (action.type){
        case FETCH_ORDER_REQUEST:
            return {...state, loading: true};
        case FETCH_ORDER_SUCCESS:
            if (action.payload === null) {
                return {...state, loading: false, ordersList: []}
            } else {
                return {...state, loading: false, ordersList: Object.keys(action.payload).map(order => ({
                        id: order,
                        ...action.payload[order]
                    }))};
            }
        case FETCH_ORDER_FAILURE:
            return {...state, loading: false, error: action.payload}
        case DELETE_ORDER_REQUEST:
            return {...state, loading: true};
        case DELETE_ORDER_SUCCESS:
            return {...state, loading: false};
        case DELETE_ORDER_FAILURE:
            return {...state, loading: false}
        default:
            return state;
    }
}

export default reducer;