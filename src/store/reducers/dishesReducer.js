import {
    ADD_DISHES_FAILURE,
    ADD_DISHES_REQUEST,
    ADD_DISHES_SUCCESS,
    DELETE_DISH_FAILURE,
    DELETE_DISH_REQUEST,
    DELETE_DISH_SUCCESS, EDIT_DISH_FAILURE,
    EDIT_DISH_REQUEST, EDIT_DISH_SUCCESS, FETCH_EDIT_DISHES_REQUEST, FETCH_EDIT_DISHES_SUCCESS,
    GET_DISHES_FAILURE,
    GET_DISHES_REQUEST,
    GET_DISHES_SUCCESS
} from "../actions/dishesActions";

const initialState = {
    dishesList: [],
    loading:false,
    error: null,
    editDishesList: [],
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_DISHES_REQUEST:
            return {...state, error: null, loading: true}
        case ADD_DISHES_SUCCESS:
            return {...state, loading: false}
        case ADD_DISHES_FAILURE:
            return {...state, loading: false, error: action.payload}
        case GET_DISHES_REQUEST:
            return {...state, loading: true}
        case GET_DISHES_SUCCESS:
            if (action.payload === null) {
                return {...state, loading: false, dishesList: []}
            } else {
                return {...state, loading: false, dishesList: Object.keys(action.payload).map(dish => ({
                        id: dish,
                        ...action.payload[dish]
                    }))};
            }
        case GET_DISHES_FAILURE:
            return {...state, loading: false, error: action.payload}
        case DELETE_DISH_REQUEST:
            return {...state, loading: true};
        case DELETE_DISH_SUCCESS:
            return {...state, loading: false};
        case DELETE_DISH_FAILURE:
            return {...state, loading: false}
        case FETCH_EDIT_DISHES_REQUEST:
            return {...state, loading: true}
        case FETCH_EDIT_DISHES_SUCCESS:
            return {...state, loading: false, editDishesList: action.payload}
        case EDIT_DISH_REQUEST:
            return {...state, loading: true};
        case EDIT_DISH_SUCCESS:
            return {...state, loading: false};
        case EDIT_DISH_FAILURE:
            return {...state, loading: false};
        default:
            return state;
    }
}

export default reducer;