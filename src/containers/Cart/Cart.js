import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import './Cart.css';
import {removeProductFromCard} from "../../store/actions/cartActions";
import deleteIcon from '../../components/UI/assets/delete.png';
import {createOrder as order} from '../../store/actions/cartActions';
import Spinner from "../../components/UI/Spinner/Spinner";
import {useHistory} from "react-router-dom";

const Cart = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const orders = useSelector(state => state.cart.orders);
    const totalSum = useSelector(state => state.cart.totalSum);
    const loading = useSelector(state => state.cart.loading);

    const createOrder = async e => {
        e.preventDefault();
        await dispatch(order({orders}));
        history.replace('/orders');
    };


    return (
        <>
            <div>
                {Object.keys(orders).map((product, i) => (
                    <div key={i} className="Order">
                        <p>{product}</p>
                        <p>x{orders[product].amount}</p>
                        <p>{orders[product].amount * orders[product].price} KGS</p>
                        <button onClick={() => dispatch(removeProductFromCard({name: product, amount: orders[product].amount}))} className="btn"><img src={deleteIcon} alt='deleteIcon' width='30px' className="click"/></button>
                    </div>
                ))}
                {loading? <Spinner/> : null}

                <div className="Total">
                    <p>Доставка: <strong>150 KGS</strong></p>
                    <p>Итого: <strong>{totalSum} KGS</strong></p>
                    <button disabled={totalSum <= 150} onClick={createOrder} className="btn btn-primary OrderButton">Order</button>
                </div>
            </div>
        </>

    );
};

export default Cart;