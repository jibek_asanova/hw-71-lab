import React, {useEffect} from 'react';
import {Link, useHistory} from "react-router-dom";
import './Dishes.css';
import {useDispatch, useSelector} from "react-redux";
import {deleteDish, getDishes} from "../../store/actions/dishesActions";
import deleteIcon from '../../components/UI/assets/delete.png';
import editIcon from '../../components/UI/assets/edit.png';

const Dishes = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const dishesList = useSelector(state => state.dishes.dishesList);

    useEffect(() => {
        dispatch(getDishes())
    }, [dispatch]);

    const removeDish = (id) => {
        dispatch(deleteDish(id));
    };

    const editDish = (id) => {
        history.replace('/edit-dish/' + id);
    };

    return (
        <div>
            <div className="title-block">
                <h2>Dishes</h2>
                <Link to="/add-dish" className="LinkAdd">Add new Dish</Link>
            </div>
            {dishesList.map(dish => (
                <div key={dish.id}
                     id={dish.id}
                     className="DishesList">
                    <img src={dish.image} alt='itemImg' width='100px' className="click"/>
                    <p>{dish.title}</p>
                    <p>{dish.price} KGS</p>
                    <button className="button1" onClick={() => editDish(dish.id)}><img src={editIcon} alt="add to cart" className="click" width='30px' /></button>
                    <button className="button1" onClick={() => removeDish(dish.id)}><img src={deleteIcon} alt="add to cart" className="click" width='30px' /></button>
                </div>
            ))}
        </div>
    );
};

export default Dishes;