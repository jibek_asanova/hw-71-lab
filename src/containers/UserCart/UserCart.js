import React from 'react';
import Menu from "../Menu/Menu";
import Cart from "../Cart/Cart";
import './UserCart.css';

const UserCart = () => {
    return (
        <div className="Container">
            <div className="Block MenuBlock">
                <h2>Menu list</h2>
                <div className="Menu">
                    <Menu/>
                </div>
            </div>
            <div className="Block Cart">
                <h2>Cart</h2>
                <div className="Cart">
                    <Cart/>
                </div>
            </div>

        </div>
    );
};

export default UserCart;