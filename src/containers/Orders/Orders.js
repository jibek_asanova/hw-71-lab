import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {deleteOrder, getOrder} from "../../store/actions/ordersActions";
import './Orders.css';
import Spinner from "../../components/UI/Spinner/Spinner";


const Orders = () => {
    const dispatch = useDispatch();
    const ordersList = useSelector(state => state.orders.ordersList);
    const loading = useSelector(state => state.orders.loading);

    useEffect(() => {
        dispatch(getOrder())
    }, [dispatch]);

    const removeDish = (id) => {
        dispatch(deleteOrder(id));
    };


    return (
        <div>
            {loading ? <Spinner/> : null}
            {ordersList.map((product) => {
                let totalSum = 150;
                return (
                    <div key={product.id} className="OrdersList">
                        {Object.keys(product.orders).map(item => {
                            totalSum += parseInt(product.orders[item].price) * parseInt(product.orders[item].amount)
                            return (
                                <div key={item} className="Order">
                                    <span>{item} </span>
                                    <span>x{product.orders[item].amount} </span>
                                    <span>{product.orders[item].price * product.orders[item].amount} KGS</span>
                                </div>
                            )
                        })}
                        <div className="totalPrice">
                            <div>
                                <p><strong>Delivery:</strong> 150 KGS</p>
                                <p><strong>Total:</strong> {totalSum} KGS</p>
                            </div>
                            <div>
                                <button className="btn btn-danger buttonDelete" onClick={() => removeDish(product.id)}>Complete Order</button>
                            </div>
                        </div>
                    </div>
                )
            })}
        </div>
    );
};

export default Orders;