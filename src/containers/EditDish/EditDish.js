import React, {useEffect, useState} from 'react';
import {useHistory} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {editDish as edit, editDishGetRequest} from "../../store/actions/dishesActions";
import Spinner from "../../components/UI/Spinner/Spinner";
import Form from "../../components/Form/Form";

const EditDish = ({match}) => {
    const history = useHistory();
    const dispatch = useDispatch();
    const matchId = match.params.id;
    const editDishList = useSelector(state => state.dishes.editDishesList);
    const loading = useSelector(state => state.dishes.loading);
    console.log('editDishList', editDishList.title);

    const [dish, setDish] = useState({
        title: '',
        price: '',
        image:''
    });

    useEffect(() => {
        dispatch(editDishGetRequest(matchId));
        setDish(prev => ({
            ...prev,
            title: editDishList.title,
            price: editDishList.price,
            image: editDishList.image
        }))
    }, [dispatch, matchId, editDishList.title, editDishList.price, editDishList.image]);

    const onInputChange = e => {
        const {name, value} = e.target;

        setDish(prev => ({
            ...prev,
            [name]: value
        }))
    };

    const editDish = async (e, id) => {
        e.preventDefault();
        console.log(id);
        await dispatch(edit(dish, id));
        history.replace('/');
    };

    let form = (
        <Form
            addNewDish={(e) => editDish(e,matchId)}
            title={dish.title}
            price={dish.price}
            image={dish.image}
            onInputChange={onInputChange}
        />
    );

    if(loading) {
        form = <Spinner/>
    }

    return (
        <div>
            <h2>Edit Dish</h2>
            {form}
        </div>
    );
};

export default EditDish;