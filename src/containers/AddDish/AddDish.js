import React, {useState} from 'react';
import Spinner from "../../components/UI/Spinner/Spinner";
import {addNewDish as addDish} from "../../store/actions/dishesActions";
import './AddDish.css';
import {useDispatch, useSelector} from "react-redux";
import Form from "../../components/Form/Form";

const AddDish = ({history}) => {
    const dispatch = useDispatch();
    const loading = useSelector(state => state.dishes.loading);

    const [dish, setDish] = useState({
        title: '',
        price: '',
        image:''
    });

    const onInputChange = e => {
        const {name, value} = e.target;

        setDish(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const addNewDish = async e => {
        e.preventDefault();
        await dispatch(addDish(dish));
        history.replace('/');
    }
    let form = (
        <Form
            addNewDish={addNewDish}
            title={dish.title}
            price={dish.price}
            image={dish.image}
            onInputChange={onInputChange}
        />
    );

    if(loading) {
        form = <Spinner/>
    }

    return (
        <div>
            <h2>Add New Dish</h2>
            {form}
        </div>
    );
};

export default AddDish;