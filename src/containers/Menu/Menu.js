import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import './Menu.css';
import cart from '../../components/UI/assets/shopping-cart.png';
import {addProductToCard} from "../../store/actions/cartActions";
import {getDishes} from '../../store/actions/dishesActions';

const Menu = () => {
    const dispatch = useDispatch();
    const dishesList = useSelector(state => state.dishes.dishesList);

    useEffect(() => {
        dispatch(getDishes())
    }, [dispatch]);

    return (
        <div>
            <div className="title-block">
            </div>
            {dishesList.map(dish => (
                <div key={dish.id}
                     id={dish.id}
                     className="DishesList Menu"
                     onClick={() => dispatch(addProductToCard(dish))}
                >
                    <img src={dish.image} alt='itemImg'  width='60px' className="click"/>
                    <p>{dish.title}</p>
                    <p>{dish.price} KGS</p>
                    <button className="button1" ><img src={cart} alt="add to cart" className="click" width='30px' /></button>
                </div>
            ))}
        </div>
    );
};

export default Menu;