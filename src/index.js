import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {createStore, applyMiddleware, combineReducers} from "redux";
import thunkMiddleware from "redux-thunk";
import dishesReducer from './store/reducers/dishesReducer';
import ordersReducer from './store/reducers/ordersReducer';
import {Provider} from "react-redux";
import {BrowserRouter} from "react-router-dom";
import cartReducer from "./store/reducers/cartReducer";

const store = createStore(combineReducers({dishes: dishesReducer, orders: ordersReducer, cart: cartReducer}), applyMiddleware(thunkMiddleware));

const app = (
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));

