import './App.css';
import {Route, Switch} from "react-router-dom";
import Layout from "./components/Layout/Layout";
import Dishes from "./containers/Dishes/Dishes";
import Orders from "./containers/Orders/Orders";
import AddDish from "./containers/AddDish/AddDish";
import EditDish from "./containers/EditDish/EditDish";
import UserCart from "./containers/UserCart/UserCart";

const App = () => (
    <Layout>
        <Switch>
            <Route path="/" exact component={Dishes}/>
            <Route path="/dishes" component={Dishes}/>
            <Route path="/add-dish" component={AddDish}/>
            <Route path="/edit-dish/:id" component={EditDish}/>
            <Route path="/userCart" component={UserCart}/>
            <Route path="/orders" component={Orders}/>
        </Switch>
    </Layout>

);

export default App;
