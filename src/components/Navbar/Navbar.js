import React from 'react';
import {Link, NavLink} from "react-router-dom";
import pizza from '../UI/assets/pizza.png';

const Navbar = () => {
    return (
        <div className="MenuBar">
            <nav className="navbar navbar-light" style={{backgroundColor: '#e3f2fd'}}>
                <nav className="navbar navbar-expand-lg navbar-light ">
                    <div className="container-fluid">
                        <Link to="/"><img src={pizza} alt="pizza-logo" width="40px" className="Logo"/></Link>
                        <a className="navbar-brand" href="/">Pizza store</a>
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"/>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                            <div className="navbar-nav">
                                <NavLink to="/dishes" className="nav-link" >Dishes</NavLink>
                                <NavLink to="/orders" className="nav-link" >Orders</NavLink>
                                <NavLink to="/userCart" className="nav-link" >User Cart</NavLink>
                            </div>
                        </div>
                    </div>
                </nav>
            </nav>
        </div>

    );
};

export default Navbar;