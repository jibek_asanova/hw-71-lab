import React from 'react';
import Navbar from "../Navbar/Navbar";
import './Layout.css';

const Layout = ({children}) => {
    return (
        <>
            <Navbar/>
            <main className="container">
                {children}
            </main>
        </>
    );
};

export default Layout;