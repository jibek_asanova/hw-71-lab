import React from 'react';

const Form = props => {
    return (
        <form className="Form" onSubmit={props.addNewDish}>
            <input
                className="Input"
                type="text"
                name="title"
                placeholder="Title"
                value={props.title}
                onChange={props.onInputChange}
            />
            <input
                className="Input"
                type="text"
                name="price"
                placeholder="Price"
                value={props.price}
                onChange={props.onInputChange}
            />
            <input
                className="Input"
                type="text"
                name="image"
                placeholder="Image"
                value={props.image}
                onChange={props.onInputChange}
            />
            <button type="submit" className="btn btn-primary">Save</button>
        </form>
    );
};

export default Form;