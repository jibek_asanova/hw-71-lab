import axios from "axios";

const axiosApi = axios.create({
    baseURL : 'https://hw-71-8780c-default-rtdb.firebaseio.com/'
});

export default axiosApi;